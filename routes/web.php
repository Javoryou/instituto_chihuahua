<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/educacion', function () {
    return view('educacion');
});
Route::get('/admisiones', function () {
    return view('admisiones');
});
Route::get('/alumno', function () {
    return view('alumno');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pago', 'PagoController@index')->name('pago');

