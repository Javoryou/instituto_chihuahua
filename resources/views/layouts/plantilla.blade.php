<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <script src="https://js.stripe.com/v3/"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <title>CIT: Centro Interactivo de Tecnologías</title>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">CIT: Centro Interactivo de Tecnologías</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="{{ url('/') }}"
                            class="{{ Request::path() === '/' ? 'nav-link active' : 'nav-link' }}">
                            Inicio
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/educacion') }}"
                            class="{{ Request::path() === 'educacion' ? 'nav-link active' : 'nav-link' }}">
                            Educación
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/admisiones') }}"
                            class="{{ Request::path() === 'admisiones' ? 'nav-link active' : 'nav-link' }}">
                            Admisiones
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/alumno') }}"
                            class="{{ Request::path() === 'alumno' ? 'nav-link active' : 'nav-link' }}">
                            Alumno
                        </a>
                    </li>

                    @guest
                    <li class="nav-item">
                        <a href="{{ url('login') }}"
                            class="{{ Request::path() === 'login' ? 'nav-link active' : 'nav-link' }}">
                            {{ __('Login') }}
                        </a>
                    </li>

                    @if (Route::has('register'))
                    <!--
                    <li class="nav-item">
                        <a href="{{ url('register') }}"
                            class="{{ Request::path() === 'register' ? 'nav-link active' : 'nav-link' }}">
                            {{ __('Register') }}
                        </a>
                    </li>
                    -->
                    @endif

                    @else
                    <li class="nav-item">
                        <a href="{{ url('/home') }}"
                            class="{{ Request::path() === 'home' ? 'nav-link active' : 'nav-link' }}">
                            Información escolar
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav><br><br>
    <!-- End Navigation -->

    <main class="py-4">
        @yield('content')
    </main>

    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Av. Miguel de Cervantes Saavedra 31125, Chihuahua, Chihuahua</p>
            <p class="m-0 text-center text-white">Centro Interactivo de Tecnologías</p>
            <p class="m-0 text-center text-white">Copyright &copy; CIT 2020</p>
        </div>
        <!-- /.container -->
    </footer>
</body>
</html>