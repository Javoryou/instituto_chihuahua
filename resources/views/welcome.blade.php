@extends('layouts.plantilla')
@section('content')

<div id="myCarousel1584477033148" class="s_carousel s_carousel_default carousel slide" data-interval="5000" data-name="Carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel1584477033148" data-slide-to="0" class="active"/>
        <li data-target="#myCarousel1584477033148" data-slide-to="1"/>
        <li data-target="#myCarousel1584477033148" data-slide-to="2"/>
    </ol>
    
    <div class="carousel-inner">
        <!-- Start Banners -->
        <div class="carousel-item oe_img_bg pt152 pb152 oe_custom_bg active" 
        style="background-image: url(/image/inicio-1.jpg); background-size: cover;" 
        data-name="Slide">
            <div class="container">
                <div class="row content">
                    <div class="carousel-content offset-md-1 col-lg-7 my-4" style="background-image: url(/image/bg-transparente.png);" >
                        <div class="s_title pb8" data-name="Title">
                            <h2 class="s_title_default" style="font-size: 45px; color: white;">
                                CIT: Centro Interactivo de Tecnologías
                            </h2>
                        </div>
                        <p class="lead" style="color: white;">
                            El mejor centro de tecnologías para el aprendizaje.<br/> 
                            Registraté y inscribete a nuestra institución.
                        </p>
                        <div class="s_btn text-left pt16 pb16" data-name="Buttons">
                            <a href="/aboutus" class="btn btn-secondary flat o_default_snippet_text">Acerca de</a>
                            <a href="/contactus" class="btn btn-primary flat o_default_snippet_text">Contáctenos</a>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <div class="carousel-item oe_custom_bg oe_img_bg pt96 pb96"
        style="background-image: url(/image/inicio-2.png); background-size: cover;" data-name="Slide">
            <div class="container">
                <div class="row content">
                    <div class="carousel-content col-lg-8 offset-lg-2 bg-black-50 text-center pt48 pb40 my-4">
                        <h2 style="font-size: 62px;" class="o_default_snippet_text">¡Ahora puedes!</h2>
                        <div class="s_hr pt8 pb32" data-name="Separator">
                            <hr class="s_hr_5px s_hr_dotted border-600 w-25 border-epsilon mx-auto text-center"/>
                        </div>
                        <br><br><br><br><br><br><br>
                        <div class="s_btn text-center pt16 pb16" data-name="Buttons">
                            <a href="/home" class="btn btn-primary">Aquí mismo</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="carousel-item oe_custom_bg oe_img_bg pt128 pb128" style="background-image: url(/image/inicio-3.jpg);" data-name="Slide">
            <div class="container">
                <div class="row content">
                    <div class="carousel-content col-lg-6 offset-lg-6 my-4" style="background-image: url(/image/bg-transparente.png);">
                        <h2 class="o_default_snippet_text" style="font-size: 45px; color: white;">Una de las mejores instituciones</h2>
                        <h4 class="o_default_snippet_text" style="color: white;">La buena escritura es simple, pero no simplista.</h4>
                        <p class="mt24 o_default_snippet_text" style="color: white;">Good copy starts with understanding how your product or service helps your customers. Simple words communicate better than big words and pompous language.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Banners-->
    </div>

    <div class="carousel-control-prev" data-target="#myCarousel1584477033148" data-slide="prev" role="img" aria-label="Anterior" title="Anterior">
        <span class="carousel-control-prev-icon"/>
        <span class="sr-only o_default_snippet_text">Anterior</span>
    </div>
    <div class="carousel-control-next" data-target="#myCarousel1584477033148" data-slide="next" role="img" aria-label="Siguiente" title="Siguiente">
        <span class="carousel-control-next-icon"/>
        <span class="sr-only o_default_snippet_text">Siguiente</span>
    </div>  
</div>

<div class="container">
    <!-- Content Inicio -->
    <div class="align-items-center my-4">
        <h1 class="s_title_thin" style="text-align: center; font-size: 62px;">
            Sobre CIT
        </h1>
        <p class="lead text-justify">
            La comunidad CIT está impulsada por un propósito compartido: Hacer un mundo mejor a 
            través de la educación, la investigación y la innovación. Somos divertidos y 
            extravagantes, élite pero no elitista, inventivo y artístico, obsesionados con los 
            números y acogedores con personas talentosas, independientemente de su procedencia.
        </p>
    </div>

    <div class="row bg-faded mt-2">
        <div class="col-4 mx-auto d-flex justify-content-center flex-wrap">
            <img src="{{ asset('image/inicio-4.png') }}"/>
        </div>
    </div>

    <div class="row align-items-center my-4">
        <div class="col">
            <h1 class="o_default_snippet_text">Misión</h1>
            <p class="lead text-justify">
                La misión de CIT es avanzar en el conocimiento y educar a los estudiantes en ciencia, 
                tecnología y otras áreas de estudio que sirvan mejor a la nación y al mundo en el siglo 
                XXI. El Instituto se compromete a generar, difundir y preservar el conocimiento, y a 
                trabajar con otros para llevar este conocimiento a los grandes desafíos del mundo. CIT 
                se dedica a proporcionar a sus estudiantes una educación que combina un estudio académico 
                riguroso y la emoción del descubrimiento con el apoyo y la estimulación intelectual de 
                una comunidad universitaria diversa. Buscamos desarrollar en cada miembro de la comunidad 
                CIT la capacidad y la pasión para trabajar de manera inteligente, creativa y efectiva 
                para el mejoramiento de la humanidad.
            </p>
        </div>
        <div class="col">
            <img src="{{ asset('image/inicio-5.jpg') }}" class="img img-fluid mx-auto" alt="Hecho por Javoryou"/>
        </div>
    </div>

    <div class="row align-items-center my-4">
        <div class="col">
            <h1 class="o_default_snippet_text">Facultad</h1>
            <p class="lead text-justify">
                Para la facultad de CIT, un poco más de 1,000, la investigación y la educación de 
                vanguardia son inseparables. Cada uno alimenta al otro. Cuando no están ocupados 
                siendo pioneros en las fronteras de sus campos, los miembros de la facultad de CIT 
                juegan un papel vital en la configuración de la vibrante comunidad del campus del 
                Instituto, como asesores, entrenadores, jefes de casa, mentores, miembros de comités 
                y mucho más.
            </p>
        </div>
        <div class="col">
            <img src="{{ asset('image/inicio-3.jpg') }}" class="img img-fluid mx-auto" alt="Hecho por Javoryou"/>
        </div>
    </div>
</div>
@endsection
