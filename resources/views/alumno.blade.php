@extends('layouts.plantilla')
@section('content')
<div class="container">
    <!-- Content Inicio -->
    <div class="align-items-center my-4">
        <h1 class="s_title_thin" style="text-align: center; font-size: 62px;">
            Alumno
        </h1>
        <p class="lead text-justify">
            Los ex alumnos de CIT (empresarios, investigadores, responsables políticos 
            y, sobre todo, líderes) han ayudado a dar forma al mundo que conocemos hoy. 
            Desde la impresión tridimensional hasta las prótesis biónicas, muchos de los 
            productos y servicios que definen el mundo moderno han surgido de las mentes 
            y manos de la brillante comunidad de antiguos alumnos de CIT (se abre en una 
            ventana nueva).
        </p>
        <p class="lead text-justify">
            Los ex alumnos de CIT representan una de las comunidades más talentosas, 
            innovadoras y en red del planeta. Muchos siguen profundamente involucrados 
            en la vida del Instituto. Son voluntarios (se abre en una nueva ventana), 
            sirven en juntas, dirigen clubes regionales de ex alumnos (se abre en una 
            nueva ventana) y apoyan generosamente (se abre en una nueva ventana) a los 
            estudiantes, profesores y personal de CIT. Como individuos, sus contribuciones 
            son extraordinarias. Como comunidad, su impacto es infinito.
        </p>
        <div class="row bg-faded mt-2">
            <div class="col-4 mx-auto d-flex justify-content-center flex-wrap">
                <img src="{{ asset('image/alumno-1.jpg') }}"/>
            </div>
        </div>
    </div>
</div>
@endsection