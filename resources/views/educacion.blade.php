@extends('layouts.plantilla')
@section('content')
<div class="container">
    <!-- Content Inicio -->
    <div class="align-items-center my-4">
        <h1 class="s_title_thin" style="text-align: center; font-size: 62px;">
            Educación
        </h1>
        <p class="lead text-justify">
            En CIT, nos deleitamos en una cultura de aprender haciendo. En 8 departamentos en dos 
            escuelas y una universidad, nuestros estudiantes combinan rigor analítico con curiosidad, 
            imaginación lúdica y un apetito para resolver los problemas más difíciles al servicio de 
            la sociedad.
        </p>
        <p class="lead text-justify">
            Nuestros estudiantes universitarios trabajan hombro con hombro con el profesorado (se 
            abre en una nueva ventana), abordan desafíos globales (se abre en una nueva ventana), 
            persiguen preguntas fundamentales y traducen ideas en acciones. El elemento vital de 
            la empresa de enseñanza e investigación del Instituto, nuestros estudiantes de posgrado 
            (se abre en una nueva ventana) y postdocs (se abre en una nueva ventana) representan 
            una de las cohortes más talentosas y diversas del mundo. Desde ciencia e ingeniería 
            hasta artes, arquitectura, humanidades, ciencias sociales y administración, y programas 
            interdisciplinarios (se abre en una nueva ventana), Ofrecemos excelencia en todos los 
            ámbitos. También somos pioneros en la educación digital, como CITx (se abre en una 
            ventana nueva), que ofrece acceso flexible a contenido riguroso para estudiantes de 
            todas las edades.
        </p>
        <div class="row bg-faded mt-2">
            <div class="col-4 mx-auto d-flex justify-content-center flex-wrap">
                <img src="{{ asset('image/educacion-1.jpg') }}"/>
            </div>
        </div>
    </div>
</div>
@endsection